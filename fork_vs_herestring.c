#define _GNU_SOURCE
#include <unistd.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <time.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

int main(int argc, char *argv[]) {
  int limit = argc > 1 ? atoi(argv[1]) : 10000;

  struct timespec t[5];
  clock_gettime(CLOCK_MONOTONIC, &t[0]);

  // pipe + vfork
  for (int i = 0; i < limit; i++) {
    int fd[2];
    pipe(fd);
    if (!vfork()) {
      write(fd[1], "hello", 5);             // the child writes 5 bytes
      _exit(0);
    }
    wait(NULL);
    char buf[5];
    read(fd[0], buf, 5);                    // read what the child wrote
    close(fd[0]);
    close(fd[1]);
    if (memcmp(buf, "hello", 5)) _exit(-1); // sanity check
  }
  clock_gettime(CLOCK_MONOTONIC, &t[1]);

  // pipe + fork
  for (int i = 0; i < limit; i++) {
    int fd[2];
    pipe(fd);
    if (!fork()) {
      write(fd[1], "hello", 5);
      _exit(0);
    }
    wait(NULL);
    char buf[5];
    read(fd[0], buf, 5);
    close(fd[0]);
    close(fd[1]);
    if (memcmp(buf, "hello", 5)) _exit(-1);
  }
  clock_gettime(CLOCK_MONOTONIC, &t[2]);

  // temp file with O_TMPFILE
  for (int i = 0; i < limit; i++) {
    int fd = open("/tmp", O_TMPFILE|O_RDWR, 0644);
    write(fd, "hello", 5);
    lseek(fd, SEEK_SET, 0);
    char buf[5];
    read(fd, buf, 5);
    close(fd);
    if (memcmp(buf, "hello", 5)) _exit(-1);
  }
  clock_gettime(CLOCK_MONOTONIC, &t[3]);

  // temp file with mkstemp + unlink
  for (int i = 0; i < limit; i++) {
    char name[] = "/tmp/XXXXXX";
    int fd = mkstemp(name);
    unlink(name);
    write(fd, "hello", 5);
    lseek(fd, SEEK_SET, 0);
    char buf[5];
    read(fd, buf, 5);
    close(fd);
    if (memcmp(buf, "hello", 5)) _exit(-1);
  }

  clock_gettime(CLOCK_MONOTONIC, &t[4]);

  long diff1 = (t[1].tv_sec * 1000000000 + t[1].tv_nsec) - (t[0].tv_sec * 1000000000 + t[0].tv_nsec),
       diff2 = (t[2].tv_sec * 1000000000 + t[2].tv_nsec) - (t[1].tv_sec * 1000000000 + t[1].tv_nsec),
       diff3 = (t[3].tv_sec * 1000000000 + t[3].tv_nsec) - (t[2].tv_sec * 1000000000 + t[2].tv_nsec),
       diff4 = (t[4].tv_sec * 1000000000 + t[4].tv_nsec) - (t[3].tv_sec * 1000000000 + t[3].tv_nsec);

  printf("=== %d iterations ===\n", limit);
  printf("vfork: %.9lfs\n", diff1 / 1000000000.0);
  printf(" fork: %.9lfs\n", diff2 / 1000000000.0);
  printf(" tmp1: %.9lfs\n", diff3 / 1000000000.0);
  printf(" tmp2: %.9lfs\n", diff4 / 1000000000.0);
}
